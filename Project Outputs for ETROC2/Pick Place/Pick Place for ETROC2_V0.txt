Altium Designer Pick and Place Locations
C:\Users\pgkounto\cernbox\Projects\ETROC\Altium\make_pad\etroc-test-card\Project Outputs for ETROC2\Pick Place\Pick Place for ETROC2_V0.txt

========================================================================================================================
File Design Information:

Date:       11/04/23
Time:       09:08
Revision:   65e4f6e59b89cce985143c7c32819217535a9b91
Variant:    No variations
Units used: mil

Designator Comment                Layer       Footprint                     Center-X(mil) Center-Y(mil) Rotation Description                                         
J7         SMA                    TopLayer    MOLEX_73251-1150              7402.953      1418.760      180      ""                                                  
IC1        LDL112PU12R            TopLayer    SON50P200X200X80-7N-D         5020.984      1021.535      180      "Integrated Circuit"                                
C60        1uF                    TopLayer    CAPC1608X87N                  5161.417      1263.780      270      "Non-polarised capacitor"                           
C61        2.2uF                  TopLayer    CAPC1608X87N                  4921.260      1263.780      270      "Non-polarised capacitor"                           
IC4        TPS7A2425DBVR          TopLayer    SOT95P280X145-5N              5041.338      1279.528      180      "Integrated Circuit"                                
R33        2k                     TopLayer    RESC1005X40N                  5606.299      1460.630      90       "Resistor - 1%"                                     
R32        100                    TopLayer    RESC1005X35N                  4909.449      1452.756      90       "Resistor - 0.1%"                                   
Q1         DMN3731U-7             TopLayer    SOT96P240X110-3N              5043.307      1437.008      0        "MOSFET (N-Channel)"                                
L7         LQW2UAS1R5J00L         TopLayer    LQW2UAS12NG00L                5532.480      1242.126      180      Inductor                                            
J6         FW-11-03-L-D-264-090-A TopLayer    SAMTEC_FW-11-03-L-D-264-090-A 5523.000      743.000       270      ""                                                  
C56        10uF                   TopLayer    CAPC2012X95N                  5401.575      1385.827      90       "Non-polarised capacitor"                           
R23        CRCW040212K4FKEDHP     TopLayer    RESC1005X40N                  5624.803      1377.953      0        Resistor                                            
R24        10k                    TopLayer    RESC1005X35N                  5542.236      1411.716      0        "Resistor - 0.1%"                                   
R20        1k                     TopLayer    RESC1005X40N                  5645.669      1460.630      270      "Resistor - 1%"                                     
R21        10k                    TopLayer    RESC1005X35N                  4948.819      1452.756      270      "Resistor - 0.1%"                                   
C55        100nF                  TopLayer    CAPC1005X55N                  5637.236      1296.716      90       "Non-polarised capacitor"                           
C58        22uF                   TopLayer    CAPC2012X135N                 5401.575      1248.032      270      "Non-polarised capacitor"                           
C57        22uF                   TopLayer    CAPC2012X135N                 5322.835      1248.032      270      "Non-polarised capacitor"                           
IC5        AP62250Z6-7            TopLayer    SOTFL50P160X60-6N             5532.992      1350.394      0        "Integrated Circuit"                                
R22        10k                    TopLayer    RESC1005X35N                  5141.732      1418.504      270      "Resistor - 0.1%"                                   
C59        1uF                    TopLayer    CAPC1608X87N                  2947.638      1506.000      270      "Non-polarised capacitor"                           
P6V0       1V0A                   TopLayer    testpoint                     783.642       1556.925      180      "TEST POINT 40MIL DIA."                             
L5         16.6nH                 TopLayer    IND_COILCRAFT_0908SQ-17N      3047.819      1376.205      180      Inductor                                            
C30        1uF                    TopLayer    CAPC1608X87N                  3012.386      1506.126      270      "Non-polarised capacitor"                           
J3         ""                     TopLayer    SAMTEC_QTE-020-01-L-D-A       2896.860      848.425       90       "Connector 40 Male (1x40) + 1x4 GND"                
J2         ""                     TopLayer    SAMTEC_QTE-020-01-L-D-A       4077.962      848.425       90       "Connector 40 Male (1x40) + 1x4 GND"                
R15        0                      TopLayer    RESC3216X65N                  990.000       1395.000      180      Resistor                                            
R14        0                      TopLayer    RESC3216X65N                  376.221       1174.252      0        Resistor                                            
R16        0                      TopLayer    RESC3216X65N                  376.024       445.197       0        Resistor                                            
R19        0                      TopLayer    RESC3216X65N                  3539.370      1503.937      90       Resistor                                            
R12        0                      TopLayer    RESC3216X65N                  1829.724      265.748       270      Resistor                                            
R13        0                      TopLayer    RESC3216X65N                  3539.370      261.811       270      Resistor                                            
R17        0                      TopLayer    RESC3216X65N                  6893.701      1255.906      0        Resistor                                            
R18        0                      TopLayer    RESC3216X65N                  1312.008      1501.968      90       Resistor                                            
J5         TSM-102-01-F-SV        TopLayer    TSM-102-02-L-SV               5520.000      340.000       90       Connector                                           
J4         0436500212             TopLayer    0436500212                    7192.913      759.842       90       Connector                                           
J1         TMM-103-06-L-D-SM      TopLayer    TMM-103-YY-XX-D-SM            5019.685      715.000       180      Connector                                           
FTG6       "Use only in PCB"      BottomLayer FIDUCIAL_TOP_C100-200         7108.181      1590.449      90       "Fiducial Target"                                   
FTG5       "Use only in PCB"      BottomLayer FIDUCIAL_TOP_C100-200         98.425        1598.425      90       "Fiducial Target"                                   
FTG4       "Use only in PCB"      BottomLayer FIDUCIAL_TOP_C100-200         98.425        98.425        90       "Fiducial Target"                                   
FTG3       "Use only in PCB"      TopLayer    FIDUCIAL_TOP_C100-200         7108.181      1590.449      90       "Fiducial Target"                                   
FTG2       "Use only in PCB"      TopLayer    FIDUCIAL_TOP_C100-200         98.425        1598.425      90       "Fiducial Target"                                   
FTG1       "Use only in PCB"      TopLayer    FIDUCIAL_TOP_C100-200         98.425        98.425        90       "Fiducial Target"                                   
C54        1uF                    TopLayer    CAPC1608X87N                  5124.921      395.315       270      "Non-polarised capacitor"                           
C33        1uF                    TopLayer    CAPC1608X87N                  5124.961      1020.787      270      "Non-polarised capacitor"                           
WS_DVDD    1V0A                   TopLayer    TESTPOINT                     4746.000      445.000       0        "TEST POINT 40MIL DIA."                             
DVDD       1V0A                   TopLayer    TESTPOINT                     4663.000      1360.000      0        "TEST POINT 40MIL DIA."                             
AVDD       1V0A                   TopLayer    TESTPOINT                     4663.000      1170.000      0        "TEST POINT 40MIL DIA."                             
R11        0                      TopLayer    RESC2012X60N                  1943.000      1195.000      0        Resistor                                            
DIN0P      "test point"           TopLayer    Test_point                    2333.662      765.555       0        ""                                                  
IC3        LDL112PU12R            TopLayer    SON50P200X200X80-7N-D         5019.646      396.811       180      "Integrated Circuit"                                
C32        4.7uF                  TopLayer    CAPC1005X55N                  4926.787      397.354       90       "Non-polarised capacitor"                           
C31        4.7uF                  TopLayer    CAPC1005X55N                  4929.016      1023.504      90       "Non-polarised capacitor"                           
C14        47pF                   TopLayer    CAPC3216X100N                 811.024       255.905       90       "Non-polarised capacitor"                           
C15        47pF                   TopLayer    CAPC3216X100N                 591.000       343.000       0        "Non-polarised capacitor"                           
C27        47pF                   TopLayer    CAPC3216X100N                 800.000       1395.000      180      "Non-polarised capacitor"                           
C20        47pF                   TopLayer    CAPC3216X100N                 1446.000      255.000       90       "Non-polarised capacitor"                           
R1         130                    TopLayer    RESC1005X40N                  646.169       1576.083      270      "Resistor - 1%"                                     
C1         100nF                  TopLayer    CAPC1005X55N                  695.000       1575.000      270      "Non-polarised capacitor"                           
C35        100nF                  TopLayer    CAPC1005X55N                  7102.362      1417.323      0        "Non-polarised capacitor"                           
C45        100nF                  TopLayer    CAPC4532X185N                 6976.378      255.905       0        "Non-polarised capacitor"                           
R10        0                      TopLayer    RESC3216X65N                  6893.701      414.370       0        Resistor                                            
U1         ETROCv2                TopLayer    ETROC2                        1567.499      863.148       90       ""                                                  
SDA        "test point"           TopLayer    Test_point                    2120.079      624.016       180      ""                                                  
SCL        "test point"           TopLayer    Test_point                    2168.307      500.000       0        ""                                                  
DIN1N      "test point"           TopLayer    Test_point                    2247.047      937.992       0        ""                                                  
DIN1P      "test point"           TopLayer    Test_point                    2247.047      858.075       0        ""                                                  
DIN0N      "test point"           TopLayer    Test_point                    2333.662      845.472       0        ""                                                  
DOUTN      "test point"           TopLayer    Test_point                    2247.047      736.221       0        ""                                                  
DOUTP      "test point"           TopLayer    Test_point                    2247.047      655.512       0        ""                                                  
CLKN       "test point"           TopLayer    Test_point                    2333.661      655.512       0        ""                                                  
CLKP       "test point"           TopLayer    Test_point                    2334.661      574.512       0        ""                                                  
P2V5       1V0A                   TopLayer    testpoint                     2098.425      1194.882      0        "TEST POINT 40MIL DIA."                             
LV_RB      1V0A                   TopLayer    testpoint                     5244.095      1456.693      0        "TEST POINT 40MIL DIA."                             
L4         16.6nH                 TopLayer    IND_COILCRAFT_0908SQ-17N      4662.000      891.000       180      Inductor                                            
L3         16.6nH                 TopLayer    IND_COILCRAFT_0908SQ-17N      4662.000      1027.913      180      Inductor                                            
L2         16.6nH                 TopLayer    IND_COILCRAFT_0908SQ-17N      4662.000      566.000       180      Inductor                                            
L1         16.6nH                 TopLayer    IND_COILCRAFT_0908SQ-17N      4662.000      705.000       180      Inductor                                            
C53        10uF                   TopLayer    CAPC2012X135N                 4562.000      891.000       270      "Non-polarised capacitor"                           
C52        10uF                   TopLayer    CAPC2012X135N                 4763.000      891.000       270      "Non-polarised capacitor"                           
C51        10uF                   TopLayer    CAPC2012X135N                 4562.000      1028.000      90       "Non-polarised capacitor"                           
C50        10uF                   TopLayer    CAPC2012X135N                 4763.000      1027.913      270      "Non-polarised capacitor"                           
C49        10uF                   TopLayer    CAPC2012X135N                 4562.000      568.000       270      "Non-polarised capacitor"                           
C48        10uF                   TopLayer    CAPC2012X135N                 4762.669      705.138       270      "Non-polarised capacitor"                           
C47        10uF                   TopLayer    CAPC2012X135N                 4562.000      706.173       90       "Non-polarised capacitor"                           
C46        10uF                   TopLayer    CAPC2012X135N                 4762.653      567.898       270      "Non-polarised capacitor"                           
R9         1k                     TopLayer    RESC1608X55N                  7137.795      248.031       270      "Resistor - 0.1%"                                   
R8         1k                     TopLayer    RESC1608X55N                  7137.795      377.953       270      "Resistor - 0.1%"                                   
R7         620k                   TopLayer    RESC1005X40N                  2665.929      1395.890      270      "Resistor - 1%"                                     
C44        100nF                  TopLayer    CAPC4532X185N                 7297.008      354.331       180      "Non-polarised capacitor"                           
C43        10pF                   TopLayer    CAPC1005X55N                  2705.299      1395.890      270      "Non-polarised capacitor"                           
IC2        LT3085EMS8E            TopLayer    SOP65P490X110-9N              2855.543      1370.575      0        "Integrated Circuit"                                
C42        10uF                   TopLayer    CAPC1608X80N                  2816.000      1474.000      180      "Non-polarised capacitor"                           
C41        2.2uF                  TopLayer    CAPC2012X140N                 2816.000      1546.000      180      "Non-polarised capacitor"                           
C40        1uF                    TopLayer    CAPC1608X87N                  3177.740      1277.780      180      "Non-polarised capacitor"                           
C29        100nF                  TopLayer    CAPC0603X33N                  1893.298      856.623       0        "Non-polarised capacitor"                           
C28        100nF                  BottomLayer CAPC0603X33N                  1761.811      522.638       90       "Non-polarised capacitor"                           
C26        100nF                  BottomLayer CAPC0603X33N                  1818.000      586.000       0        "Non-polarised capacitor"                           
C25        1uF                    TopLayer    CAPC1005X55N                  1880.913      594.488       180      "Non-polarised capacitor"                           
C24        22nF                   BottomLayer CAPC0603X33N                  1822.000      1150.000      0        "Non-polarised capacitor"                           
C23        22nF                   BottomLayer CAPC0603X33N                  1805.000      1232.000      0        "Non-polarised capacitor"                           
C22        100nF                  BottomLayer CAPC0603X33N                  1820.000      1192.000      0        "Non-polarised capacitor"                           
C21        100nF                  BottomLayer CAPC0603X33N                  1824.000      728.000       0        "Non-polarised capacitor"                           
C19        100nF                  BottomLayer CAPC0603X33N                  1809.063      633.858       0        "Non-polarised capacitor"                           
C39        10uF                   TopLayer    CAPC3225X280N                 3204.441      1375.575      180      "Non-polarised capacitor"                           
R6         4k7                    TopLayer    RESC1005X40N                  2048.228      620.079       90       "Resistor - 1%"                                     
R5         4k7                    TopLayer    RESC1005X40N                  2097.441      517.716       90       "Resistor - 1%"                                     
R4         100                    TopLayer    RESC0603X28N                  1938.980      764.764       90       "Resistor - 1%"                                     
C18        100nF                  BottomLayer CAPC0603X33N                  1822.000      683.000       0        "Non-polarised capacitor"                           
C17        1uF                    TopLayer    CAPC1005X55N                  1880.913      651.575       180      "Non-polarised capacitor"                           
R3         100                    TopLayer    RESC0603X28N                  1904.531      715.551       90       "Resistor - 1%"                                     
C16        100nF                  BottomLayer CAPC0603X33N                  1810.000      490.000       90       "Non-polarised capacitor"                           
C13        100nF                  BottomLayer CAPC0603X33N                  1800.000      424.000       0        "Non-polarised capacitor"                           
C12        1uF                    TopLayer    CAPC1005X55N                  1804.142      420.276       0        "Non-polarised capacitor"                           
C11        22nF                   BottomLayer CAPC0603X33N                  1853.347      926.181       90       "Non-polarised capacitor"                           
C10        22nF                   BottomLayer CAPC0603X33N                  1805.126      850.394       90       "Non-polarised capacitor"                           
C9         22nF                   BottomLayer CAPC0603X33N                  1791.000      981.000       0        "Non-polarised capacitor"                           
C8         100nF                  BottomLayer CAPC0603X33N                  1805.126      925.197       90       "Non-polarised capacitor"                           
C7         100nF                  BottomLayer CAPC0603X33N                  1821.000      1021.000      0        "Non-polarised capacitor"                           
C6         100nF                  BottomLayer CAPC0603X33N                  1845.472      804.134       270      "Non-polarised capacitor"                           
C5         100nF                  BottomLayer CAPC0603X33N                  1803.000      789.000       0        "Non-polarised capacitor"                           
C4         100nF                  BottomLayer CAPC0603X33N                  1846.457      1055.118      180      "Non-polarised capacitor"                           
C3         1uF                    TopLayer    CAPC1005X55N                  1883.866      940.945       180      "Non-polarised capacitor"                           
RT1        2.7k                   TopLayer    THERMC1608X95N                1687.992      375.000       0        "Thermistor NTC (Negative Temperature Coefficient)" 
RT2        2.7k                   TopLayer    THERMC1608X95N                1670.276      1341.535      0        "Thermistor NTC (Negative Temperature Coefficient)" 
R2         1k                     TopLayer    RESC1005X40N                  464.772       1400.512      270      "Resistor - 1%"                                     
FL1        GALI-S66+              TopLayer    GALIS66                       476.661       1535.079      90       Filter                                              
C2         100nF                  TopLayer    CAPC1005X55N                  504.142       1400.512      90       "Non-polarised capacitor"                           
WS_AVDD    1V0A                   TopLayer    testpoint                     4663.000      1265.000      0        "TEST POINT 40MIL DIA."                             
